// Copyright � 2019 Drovean (Samael Volkihar)

#pragma once

#include "CoreMinimal.h"
#include "AFStructs.generated.h"

USTRUCT(BlueprintType)
struct FPigCustomisationData
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data", meta = (ClampMin = 0.005f, ClampMax = 0.01f))
	float MeshScale;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data", meta = (ClampMin = 0.005f, ClampMax = 0.01f))
	float MatScale;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data")
	FVector NoiseOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data")
	bool Invert;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data")
	float HueA;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data")
	float SatA;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data")
	float ValA;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data")
	float OpacityA;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data")
	float HueB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data")
	float SatB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data")
	float ValB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pig Data")
	float OpacityB;

	static const float MeshScale_Min;
	static const float MeshScale_Max;
	static const float MatScale_Min;
	static const float MatScale_Max;
	static const float NoiseOffset_Min;
	static const float NoiseOffset_Max;
	static const float Hue_Min;
	static const float Hue_Max;
	static const float Sat_Min;
	static const float Sat_Max;
	static const float Val_Min;
	static const float Val_Max;
	static const float Opacity_Min;
	static const float Opacity_Max;

	bool IsEnabled()
	{ return (OpacityA > 0 || OpacityB > 0); }

	FPigCustomisationData() :
		MeshScale(1), MatScale(0.005f), NoiseOffset(FVector::ZeroVector), Invert(false),
		HueA(0.f), SatA(1.f), ValA(0.255f), OpacityA(0.65f),
		HueB(25.5f), SatB(0.5f), ValB(0.255f), OpacityB(0.95f) {}

	FPigCustomisationData(float meshscale, float matscale, FVector noise_offset, bool invert, float hue_a, float sat_a, float val_a, float opacity_a, float hue_b, float sat_b, float val_b, float opacity_b) :
		MeshScale(meshscale), MatScale(matscale), NoiseOffset(noise_offset), Invert(invert),
		HueA(hue_a), SatA (sat_a), ValA(val_a), OpacityA(opacity_a),
		HueB(hue_b), SatB (sat_b), ValB(val_b), OpacityB(opacity_b) {}
	
	//static const UPigMaterialData Disabled = UPigMaterialData(0.005f, FVector::ZeroVector, 0.f, 1.f, 0.255f, 0.f, 0.f, 1.f, 0.255f, 0.f, false);
};