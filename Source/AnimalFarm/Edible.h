// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Biteable.h"
#include "Edible.generated.h"

UCLASS()
class ANIMALFARM_API AEdible : public AActor, public IBiteable
{
	GENERATED_BODY()

public:	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FString name;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	int score_value;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float time_value;

	AEdible();

	virtual EBiteType GetBiteResponse_Implementation() override;

	virtual void ReactToEaten_Implementation(class AAnimalFarmCharacter* eaten_by) override;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	UStaticMeshComponent* GetStaticMesh();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	UDestructibleComponent* GetDestructibleMesh();
};
