// Copyright � 2019 Drovean (Samael Volkihar)

#pragma once

#include "CoreMinimal.h"
#include "AnimalFarmCharacter.h"
#include "AFStructs.h"
#include "Pig.generated.h"

UCLASS()
class ANIMALFARM_API APig : public AAnimalFarmCharacter
{
	GENERATED_BODY()
	
protected:
	UPROPERTY()
	UMaterialInstanceDynamic* matdyn_body;
	
	UPROPERTY()
	UMaterialInstanceDynamic* matdyn_hair;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool randomise_pattern_on_spawn;

	// Set to a negative value to randomise
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int pattern_seed = -1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool randomise_name_on_spawn;

	// Set to a negative value to randomise
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int name_seed = -1;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TArray<FString> names = {"Babe", "Chewbacon", "Hamish", "Hamlet", "Hamm", "Hogzilla", "Frank", "Kevin Bacon", "Miss Piggy", "Napoleon", "Plopper", "Porkchop", "Porky Pig", "Pumbaa", "Slim", "Snowball", "Squealer"};

public:
	APig();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void RandomiseName(int seed = -1);

	UFUNCTION(BlueprintCallable)
	void RandomiseCustomisation(int seed = -1, float plain_chance = 25.f, float two_col_chance = 25.f);

	UFUNCTION(BlueprintCallable)
	void SetPatternOneColour(float patch_scale, FVector patch_noise_offset, FLinearColor patch_colour = FLinearColor(0.03125f, 0.02125f, 0.019f, 1.0f), float patch_opacity = 0.9f, bool patch_invert = false, bool patch_a = true);

	UFUNCTION(BlueprintCallable)
	void SetPatternTwoColours(float patch_scale, FVector patch_noise_offset, FLinearColor patch_colour_a = FLinearColor(0.03125f, 0.02125f, 0.019f, 1.0f), float patch_opacity_a = 0.9f, FLinearColor patch_colour_b = FLinearColor(0.03125f, 0.02125f, 0.019f, 1.0f), float patch_opacity_b = 0.9f, bool patch_invert = false);

	UFUNCTION(BlueprintCallable)
	void SetCustomisationData(FPigCustomisationData new_pattern);

	UFUNCTION(BlueprintCallable)
	void SetCustomisationFromString(FString new_pattern_string);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	FPigCustomisationData GetCustomisationData();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	FString GetCustomisationString();

	UFUNCTION(BlueprintCallable)
	void SetPatchScale(float patch_scale);

	UFUNCTION(BlueprintCallable)
	void SetPatchNoiseOffset(FVector patch_noise_offset);

	UFUNCTION(BlueprintCallable)
	void SetPatchInvert(bool patch_invert);

	UFUNCTION(BlueprintCallable)
	void SetPatchColourFromRGB(float r, float g, float b, bool set_a = true);

	UFUNCTION(BlueprintCallable)
	void SetPatchColourFromHSV(float hue, float sat, float val, bool set_a = true);

	UFUNCTION(BlueprintCallable)
	void SetPatchColour(FLinearColor patch_colour, bool set_a = true);

	UFUNCTION(BlueprintCallable)
	void SetPatchOpacity(float patch_opacity, bool set_a = true);

	UFUNCTION(BlueprintCallable)
	void SetUsePattern(bool use_pattern);

	virtual void InteractWithAnimalOnHit(AAnimalFarmCharacter* animal) override;
};
