// Copyright � 2019 Drovean (Samael Volkihar)

#include "AnimalFarmCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "TimerManager.h"
#include "Biteable.h"

AAnimalFarmCharacter::AAnimalFarmCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	base_turn_rate = 45.f;
	base_look_up_rate = 45.f;
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 300.0f, 0.0f); 
	GetCharacterMovement()->MaxWalkSpeed = max_walk_speed;
	GetCharacterMovement()->AirControl = 0.2f;

	camera_boom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	camera_boom->SetupAttachment(RootComponent);
	camera_boom->TargetArmLength = zoom_boom_target_length;
	camera_boom->bUsePawnControlRotation = true;

	camera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	camera->SetupAttachment(camera_boom, USpringArmComponent::SocketName);
	camera->bUsePawnControlRotation = false;

	bite_collider = CreateDefaultSubobject<USphereComponent>(TEXT("BiteCollider"));
	bite_collider->SetupAttachment(GetMesh(), TEXT("MouthSocket"));
	bite_collider->InitSphereRadius(28.f);
}

void AAnimalFarmCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Turn", this, &AAnimalFarmCharacter::Turn);
	PlayerInputComponent->BindAxis("TurnRate", this, &AAnimalFarmCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &AAnimalFarmCharacter::LookUpOrZoom);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AAnimalFarmCharacter::LookUpAtRate);
	PlayerInputComponent->BindAxis("Zoom", this, &AAnimalFarmCharacter::AdjustZoom);

	PlayerInputComponent->BindAction("ZoomControl", IE_Pressed, this, &AAnimalFarmCharacter::ActivateZoomControl);
	PlayerInputComponent->BindAction("ZoomControl", IE_Released, this, &AAnimalFarmCharacter::DeactivateZoomControl);

	PlayerInputComponent->BindAxis("MoveForward", this, &AAnimalFarmCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AAnimalFarmCharacter::MoveRight);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AAnimalFarmCharacter::JumpPressed);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AAnimalFarmCharacter::JumpReleased);

	PlayerInputComponent->BindAction("Charge", IE_Pressed, this, &AAnimalFarmCharacter::ChargePressed);
	PlayerInputComponent->BindAction("Charge", IE_Released, this, &AAnimalFarmCharacter::ChargeReleased);

	PlayerInputComponent->BindAction("Bite", IE_Pressed, this, &AAnimalFarmCharacter::BitePressed);
	PlayerInputComponent->BindAction("Bite", IE_Released, this, &AAnimalFarmCharacter::BiteReleased);
}

void AAnimalFarmCharacter::Turn(float Val)
{ AddControllerYawInput(Val); }

void AAnimalFarmCharacter::TurnAtRate(float Rate)
{ Turn(Rate * base_turn_rate * GetWorld()->GetDeltaSeconds()); }

void AAnimalFarmCharacter::LookUpOrZoom(float Val)
{
	if (!zoom_control_active)
		AddControllerPitchInput(Val);
	else
		AdjustZoom(Val);
}

void AAnimalFarmCharacter::LookUpAtRate(float Rate)
{
	if (!zoom_control_active)
		AddControllerPitchInput(Rate * base_look_up_rate * GetWorld()->GetDeltaSeconds());
	else
		AdjustZoom(Rate * GetWorld()->GetDeltaSeconds());
}

void AAnimalFarmCharacter::AdjustZoom(float Val)
{ zoom_boom_target_length = FMath::Clamp(zoom_boom_target_length + Val * zoom_adjust_rate, min_zoom_dist, max_zoom_dist); }

void AAnimalFarmCharacter::ActivateZoomControl()
{ zoom_control_active = true; }

void AAnimalFarmCharacter::DeactivateZoomControl()
{ zoom_control_active = false; }

void AAnimalFarmCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AAnimalFarmCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AAnimalFarmCharacter::JumpPressed()
{ GetWorldTimerManager().SetTimer(jump_timer_handle, this, &AAnimalFarmCharacter::Jump, jump_delay); should_enter_jump_animation = true; }

void AAnimalFarmCharacter::JumpReleased()
{ GetWorldTimerManager().ClearTimer(jump_timer_handle); should_enter_jump_animation = false; }

void AAnimalFarmCharacter::Jump()
{ LaunchCharacter((GetLaunchVector() * jump_strength), false, false); should_enter_jump_animation = false; }

FVector AAnimalFarmCharacter::GetLaunchVector()
{ return (GetActorForwardVector() + FVector::UpVector).GetSafeNormal(); }

void AAnimalFarmCharacter::ChargePressed()
{ GetCharacterMovement()->MaxWalkSpeed = max_run_speed; is_running = true; }

void AAnimalFarmCharacter::ChargeReleased()
{ GetCharacterMovement()->MaxWalkSpeed = max_walk_speed; is_running = false; }

void AAnimalFarmCharacter::BitePressed()
{
	uint8 things_to_eat = 0;
	TArray<AActor*> things_to_carry;
	TArray<AActor*> things_to_drag;
	TArray<AActor*> things_to_damage;

	TArray<AActor*> actors_in_bite_range;
	bite_collider->GetOverlappingActors(actors_in_bite_range);

	bite_state = EBiteType::NONE;

	for (AActor* bitten_actor : actors_in_bite_range)
	{
		IBiteable* biteable_interface = Cast<IBiteable>(bitten_actor);
		
		if (biteable_interface)
		{
			switch (IBiteable::Execute_GetBiteResponse(bitten_actor))
			{
			case EBiteType::EAT:
				++things_to_eat;
				break;
			case EBiteType::CARRY:
				things_to_carry.Add(bitten_actor);
				break;
			case EBiteType::DRAG:
				things_to_drag.Add(bitten_actor);
				break;
			case EBiteType::DAMAGE:
				things_to_damage.Add(bitten_actor);
				break;
			}
		}
	}

	if (things_to_carry.Num() > 0)
	{
		// sort to find closest

		IBiteable::Execute_ReactToCarry(things_to_carry[0], this);
	}
	else if (things_to_drag.Num() > 0)
	{
		// sort to find closest

		IBiteable::Execute_ReactToDrag(things_to_carry[0], this);
	}
	else if (things_to_eat > 0)
	{
		bite_state = EBiteType::EAT;
		should_enter_eat_animation = true;
		GetCharacterMovement()->MaxWalkSpeed = 0;
		GetWorldTimerManager().SetTimer(eat_timer_handle, this, &AAnimalFarmCharacter::TryEat, eat_start_time);
	}
	else
	{
		bite_state = EBiteType::DAMAGE;
		should_enter_bite_animation = true;
		for (AActor* bitten_actor : things_to_damage)
		{
			IBiteable::Execute_ReactToBite(bitten_actor, this);
		}
	}
}

void AAnimalFarmCharacter::BiteReleased()
{
	should_enter_bite_animation = false;

	switch (bite_state)
	{
	case EBiteType::DAMAGE:
		bite_state = EBiteType::NONE;
	case EBiteType::EAT:
		FinishEating();
		break;
	case EBiteType::CARRY:
	case EBiteType::DRAG:
		if (held_actor)
		{
			IBiteable* biteable_interface = Cast<IBiteable>(held_actor);
			if (biteable_interface)
				IBiteable::Execute_ReactToDrop(held_actor, this);
		}
		break;
	}
}

void AAnimalFarmCharacter::TryEat()
{
	TArray<AActor*> actors_in_bite_range;
	bite_collider->GetOverlappingActors(actors_in_bite_range);
	int edibles = 0;
	AActor* closest_edible = nullptr;
	float closest_dist_sqd = FLT_MAX;

	for (AActor* bitten_actor : actors_in_bite_range)
	{
		IBiteable* biteable_interface = Cast<IBiteable>(bitten_actor);

		if (biteable_interface && IBiteable::Execute_GetBiteResponse(bitten_actor) == EBiteType::EAT)
		{
			++edibles;
			float dist_sqd = FVector::DistSquared(bite_collider->GetComponentLocation(), bitten_actor->GetActorLocation());
			if (dist_sqd < closest_dist_sqd)
			{
				closest_dist_sqd = dist_sqd;
				closest_edible = bitten_actor;
			}
		}
	}

	if (closest_edible != nullptr)
	{
		IBiteable::Execute_ReactToEaten(closest_edible, this);

		if (edibles > 1)
		{
			GetWorldTimerManager().SetTimer(eat_timer_handle, this, &AAnimalFarmCharacter::TryEat, eat_start_time);
			return;
		}
	}

	FinishEating();
}

void AAnimalFarmCharacter::FinishEating()
{
	should_enter_eat_animation = false;
	bite_state = EBiteType::NONE;
	GetCharacterMovement()->MaxWalkSpeed = is_running ? max_run_speed : max_walk_speed;
}

void AAnimalFarmCharacter::Pickup(AActor* actor, bool drag)
{ should_enter_bite_animation = false; held_actor = actor; bite_state = drag ? EBiteType::DRAG : EBiteType::CARRY; }

void AAnimalFarmCharacter::Drop()
{ held_actor = nullptr; bite_state = EBiteType::NONE; }

void AAnimalFarmCharacter::SetName(FString new_name)
{ name = new_name; }

void AAnimalFarmCharacter::SetViewMode(bool view)
{ GetCharacterMovement()->MaxWalkSpeed = view ? 0 : is_running ? max_run_speed : max_walk_speed; }

void AAnimalFarmCharacter::BeginPlay()
{
	Super::BeginPlay();

	GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &AAnimalFarmCharacter::OnHit);
}

void AAnimalFarmCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (camera_boom->TargetArmLength != zoom_boom_target_length)
		camera_boom->TargetArmLength = FMath::FInterpTo(camera_boom->TargetArmLength, zoom_boom_target_length, DeltaTime, zoom_adjust_rate);
	
	if (held_actor)
	{
		IBiteable* biteable_interface = Cast<IBiteable>(held_actor);
		if (biteable_interface)
			IBiteable::Execute_ReactToDrag(held_actor, this);
	}
}

void AAnimalFarmCharacter::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	AAnimalFarmCharacter* animal = Cast<AAnimalFarmCharacter>(OtherActor);
	if (animal) InteractWithAnimalOnHit(animal);
}

void AAnimalFarmCharacter::InteractWithAnimalOnHit(AAnimalFarmCharacter* animal)
{}