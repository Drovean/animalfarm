// Copyright � 2019 Drovean (Samael Volkihar)

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AFEnums.h"
#include "AnimalFarmCharacter.generated.h"


UCLASS(config=Game)
class AAnimalFarmCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* camera_boom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* bite_collider;

public:
	AAnimalFarmCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString name;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	float base_turn_rate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	float base_look_up_rate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	float min_zoom_dist = 200;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	float max_zoom_dist = 600;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float zoom_adjust_rate = 45;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	bool zoom_control_active;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float zoom_boom_target_length = 400;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement)
	float max_walk_speed = 300.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement)
	float max_run_speed = 600.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	bool is_running = false;
	
	FTimerHandle jump_timer_handle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement)
	float jump_delay = 0.25f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement)
	float jump_strength = 500.f;

	FTimerHandle eat_timer_handle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Eating)
	float eat_start_time = 0.25f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Eating)
	float eat_finish_time = 0.25f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Animation)
	bool should_enter_jump_animation;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Animation)
	bool should_enter_bite_animation;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Animation)
	bool should_enter_eat_animation;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Biteable)
	EBiteType bite_state;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Biteable)
	AActor* held_actor;

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	/**
	 * Called via input to turn by an absolute deta
	 * @param Val Amount to add to Yaw. 
	 */
	void Turn(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn by an absolute deta
	 * @param Val Amount to add to Pitch or Zoom.
	 */
	void LookUpOrZoom(float Val);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/**
	 * Called via input to adjust camera zoom at a given rate.
	 * @param Val	Amount to add to Zoom.
	 */
	void AdjustZoom(float Val);

	UFUNCTION()
	void ActivateZoomControl();

	UFUNCTION()
	void DeactivateZoomControl();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	UFUNCTION()
	virtual void JumpPressed();

	UFUNCTION()
	virtual void JumpReleased();

	virtual void Jump() override;

	UFUNCTION()
	FVector GetLaunchVector();

	UFUNCTION()
	virtual void ChargePressed();

	UFUNCTION()
	virtual void ChargeReleased();

	UFUNCTION()
	virtual void BitePressed();

	UFUNCTION()
	virtual void BiteReleased();

	UFUNCTION()
	virtual void TryEat();

protected:
	UFUNCTION()
	virtual void FinishEating();

	UFUNCTION()
	virtual void Pickup(AActor* actor, bool drag);

	UFUNCTION()
	virtual void Drop();

public:
	UFUNCTION(BlueprintCallable)
	virtual void SetName(FString new_name);

	UFUNCTION(BlueprintCallable)
	virtual void SetViewMode(bool view);

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	virtual void InteractWithAnimalOnHit(AAnimalFarmCharacter* animal);
	
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return camera_boom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return camera; }
	/** Returns BiteCollider subobject **/
	FORCEINLINE USphereComponent* GetBiteCollider() { return bite_collider; }
};

