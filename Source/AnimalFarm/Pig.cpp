// Copyright � 2019 Drovean (Samael Volkihar)

#include "Pig.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "AFBPFL.h"

APig::APig()
{
	name = "Porkchop";
	GetCapsuleComponent()->InitCapsuleSize(52, 52);
	GetMesh()->SetRelativeLocation({ 0, 0, -52 });
}

void APig::BeginPlay()
{
	Super::BeginPlay();

	USkeletalMeshComponent* mesh_comp = GetMesh();

	matdyn_body = UMaterialInstanceDynamic::Create(mesh_comp->GetMaterial(0), this);
	matdyn_hair = UMaterialInstanceDynamic::Create(mesh_comp->GetMaterial(1), this);

	mesh_comp->SetMaterial(0, matdyn_body);
	mesh_comp->SetMaterial(1, matdyn_hair);

	if (randomise_name_on_spawn)
		RandomiseName(name_seed);
	if (randomise_pattern_on_spawn)
		RandomiseCustomisation(pattern_seed, 0);
}

void APig::RandomiseName(int seed)
{
	if (seed < 0)
		seed = FMath::Rand();
	
	FRandomStream rs;
	rs.Initialize(seed);

	name = names[rs.RandRange(0, names.Num() - 1)];
}

void APig::RandomiseCustomisation(int seed, float plain_chance, float two_col_chance)
{
	if (seed < 0)
		seed = FMath::Rand()/* - (FDateTime::Now().GetMillisecond() * FDateTime::Now().GetSecond())*/;
	
	FRandomStream rs;
	rs.Initialize(seed);

	// Use a weighted random for pig size with lower chance at the extremes
	float scale_alpha = FMath::Asin(rs.FRandRange(-1.f, 1.f)) / PI + .5f;
	SetActorScale3D(FVector(FMath::Lerp(FPigCustomisationData::MeshScale_Min, FPigCustomisationData::MeshScale_Max, scale_alpha)));

	float pattern_type = rs.FRandRange(0, 100);

	// Skip plain chance if already patternless (e.g. Randomise on Paint-A-Pig mode)
	if (matdyn_body->K2_GetScalarParameterValue("UsePattern") == 0)
		pattern_type = FMath::Clamp(pattern_type + plain_chance, plain_chance, 100.f);

	if (pattern_type < plain_chance) // 25% chance of a plain pig
		SetUsePattern(false);
	else
	{
		bool two_coloured = (rs.RandRange(0, 1) == 1);

		// Make larger patched (min scale) more likely by squaring random result
		float patch_scale = FMath::Lerp(FPigCustomisationData::MatScale_Max, FPigCustomisationData::MatScale_Min, FMath::Square(rs.FRand()));
		FVector patch_noise_offset = FVector(
			rs.FRandRange(FPigCustomisationData::NoiseOffset_Min, FPigCustomisationData::NoiseOffset_Max),
			rs.FRandRange(FPigCustomisationData::NoiseOffset_Min, FPigCustomisationData::NoiseOffset_Max),
			rs.FRandRange(FPigCustomisationData::NoiseOffset_Min, FPigCustomisationData::NoiseOffset_Max));
		FLinearColor patch_colour_a = FLinearColor(
			rs.FRandRange(FPigCustomisationData::Hue_Min, FPigCustomisationData::Hue_Max),
			rs.FRandRange(FPigCustomisationData::Sat_Min, FPigCustomisationData::Sat_Max),
			rs.FRandRange(FPigCustomisationData::Val_Min, FPigCustomisationData::Val_Max),
			1).HSVToLinearRGB();
		float patch_opacity_a = rs.FRandRange(FPigCustomisationData::Opacity_Min, FPigCustomisationData::Opacity_Max);
		bool patch_invert = (rs.RandRange(0, 1) == 1);

		if (pattern_type < 100 - two_col_chance) // 50% chance of one coloured pig
		{
			bool patch_a = (rs.RandRange(0, 1) == 1);
			SetPatternOneColour(patch_scale, patch_noise_offset, patch_colour_a, patch_opacity_a, patch_invert, patch_a);
		}
		else // 25% chance of two coloured pig
		{
			FLinearColor patch_colour_b = FLinearColor(
				rs.FRandRange(FPigCustomisationData::Hue_Min, FPigCustomisationData::Hue_Max),
				rs.FRandRange(FPigCustomisationData::Sat_Min, FPigCustomisationData::Sat_Max),
				rs.FRandRange(FPigCustomisationData::Val_Min, FPigCustomisationData::Val_Max),
				1).HSVToLinearRGB();
			float patch_opacity_b = rs.FRandRange(FPigCustomisationData::Opacity_Min, FPigCustomisationData::Opacity_Max);

			SetPatternTwoColours(patch_scale, patch_noise_offset, patch_colour_a, patch_opacity_a, patch_colour_b, patch_opacity_b, patch_invert);
		}
	}
}

void APig::SetPatternOneColour(float patch_scale, FVector patch_noise_offset, FLinearColor patch_colour, float patch_opacity, bool patch_invert, bool patch_a)
{
	SetPatchScale(patch_scale);
	SetPatchNoiseOffset(patch_noise_offset);
	SetPatchInvert(patch_invert);
	SetPatchColour(patch_colour, patch_a);
	SetPatchOpacity(patch_opacity, patch_a);
	SetPatchOpacity(0, !patch_a);
	SetUsePattern(true);
}

void APig::SetPatternTwoColours(float patch_scale, FVector patch_noise_offset, FLinearColor patch_colour_a, float patch_opacity_a, FLinearColor patch_colour_b, float patch_opacity_b, bool patch_invert)
{
	SetPatchScale(patch_scale);
	SetPatchNoiseOffset(patch_noise_offset);
	SetPatchInvert(patch_invert);
	SetPatchColour(patch_colour_a, true);
	SetPatchOpacity(patch_opacity_a, true);
	SetPatchColour(patch_colour_b, false);
	SetPatchOpacity(patch_opacity_b, false);
	SetUsePattern(true);
}

void APig::SetCustomisationData(FPigCustomisationData new_pattern)
{
	SetActorScale3D(FVector(new_pattern.MeshScale));
	SetPatchScale(new_pattern.MatScale);
	SetPatchNoiseOffset(new_pattern.NoiseOffset);
	SetPatchInvert(new_pattern.Invert);
	SetPatchColourFromHSV(new_pattern.HueA, new_pattern.SatA, new_pattern.ValA, true);
	SetPatchOpacity(new_pattern.OpacityA, true);
	SetPatchColourFromHSV(new_pattern.HueB, new_pattern.SatB, new_pattern.ValB, false);
	SetPatchOpacity(new_pattern.OpacityB, false);
	SetUsePattern(new_pattern.OpacityA > 0 || new_pattern.OpacityB > 0);
}

void APig::SetCustomisationFromString(FString new_pattern_string)
{ SetCustomisationData(UAFBPFL::HexStringToPigCustomisationData(new_pattern_string)); }

FPigCustomisationData APig::GetCustomisationData()
{
	FPigCustomisationData pig_data;

	pig_data.MeshScale = GetActorScale().Z;
	pig_data.MatScale = matdyn_body->K2_GetScalarParameterValue("PatchScale");
	FLinearColor temp = matdyn_body->K2_GetVectorParameterValue("PatchNoiseOffset");
	pig_data.NoiseOffset = FVector(temp.R, temp.G, temp.B);
	pig_data.Invert = matdyn_body->K2_GetScalarParameterValue("PatchInvert") == 1;
	temp = matdyn_body->K2_GetVectorParameterValue("PatchColourA").LinearRGBToHSV();
	pig_data.HueA = temp.R;
	pig_data.SatA = temp.G;
	pig_data.ValA = temp.B;
	pig_data.OpacityA = matdyn_body->K2_GetScalarParameterValue("PatchOpacityA");
	temp = matdyn_body->K2_GetVectorParameterValue("PatchColourB").LinearRGBToHSV();
	pig_data.HueB = temp.R;
	pig_data.SatB = temp.G;
	pig_data.ValB = temp.B;
	pig_data.OpacityB = matdyn_body->K2_GetScalarParameterValue("PatchOpacityB");
	return pig_data;
}

FString APig::GetCustomisationString()
{	return UAFBPFL::PigCustomisationValsToHexString(GetCustomisationData()); }

void APig::SetPatchScale(float patch_scale)
{
	matdyn_body->SetScalarParameterValue("PatchScale", patch_scale);
	matdyn_hair->SetScalarParameterValue("PatchScale", patch_scale);
}

void APig::SetPatchNoiseOffset(FVector patch_noise_offset)
{
	matdyn_body->SetVectorParameterValue("PatchNoiseOffset", patch_noise_offset);
	matdyn_hair->SetVectorParameterValue("PatchNoiseOffset", patch_noise_offset);
}

void APig::SetPatchInvert(bool patch_invert)
{
	matdyn_body->SetScalarParameterValue("PatchInvert", patch_invert);
	matdyn_hair->SetScalarParameterValue("PatchInvert", patch_invert);
}

void APig::SetPatchColourFromRGB(float r, float g, float b, bool set_a)
{
	FLinearColor patch_colour = FLinearColor(r, g, b, 1);
	matdyn_body->SetVectorParameterValue(set_a ? "PatchColourA" : "PatchColourB", patch_colour);
	matdyn_hair->SetVectorParameterValue(set_a ? "PatchColourA" : "PatchColourB", patch_colour);
}

void APig::SetPatchColourFromHSV(float hue, float sat, float val, bool set_a)
{
	FLinearColor patch_colour = FLinearColor(hue, sat, val, 1).HSVToLinearRGB();
	matdyn_body->SetVectorParameterValue(set_a ? "PatchColourA" : "PatchColourB", patch_colour);
	matdyn_hair->SetVectorParameterValue(set_a ? "PatchColourA" : "PatchColourB", patch_colour);
}

void APig::SetPatchColour(FLinearColor patch_colour, bool set_a)
{
	patch_colour.A = 1;
	matdyn_body->SetVectorParameterValue(set_a ? "PatchColourA" : "PatchColourB", patch_colour);
	matdyn_hair->SetVectorParameterValue(set_a ? "PatchColourA" : "PatchColourB", patch_colour);
}

void APig::SetPatchOpacity(float patch_opacity, bool set_a)
{
	matdyn_body->SetScalarParameterValue(set_a ? "PatchOpacityA" : "PatchOpacityB", patch_opacity);
	matdyn_hair->SetScalarParameterValue(set_a ? "PatchOpacityA" : "PatchOpacityB", patch_opacity);
}

void APig::SetUsePattern(bool use_pattern)
{
	matdyn_body->SetScalarParameterValue("UsePattern", use_pattern);
	matdyn_hair->SetScalarParameterValue("UsePattern", use_pattern);
}

void APig::InteractWithAnimalOnHit(AAnimalFarmCharacter* animal)
{
	// Check other animal is in front of this one
	if ((FVector::DotProduct(GetActorForwardVector(), (animal->GetActorLocation() - GetActorLocation()).GetSafeNormal())) > 0)
	{
		if (GetCharacterMovement()->bWantsToCrouch == false)
		{
			animal->LaunchCharacter(GetLaunchVector() * GetVelocity().Size(), false, false);
		}
	}
}