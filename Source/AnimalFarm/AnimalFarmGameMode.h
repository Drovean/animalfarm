// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AnimalFarmGameMode.generated.h"

UCLASS(minimalapi)
class AAnimalFarmGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAnimalFarmGameMode();
};



