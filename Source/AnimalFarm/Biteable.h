// Copyright � 2019 Drovean (Samael Volkihar)

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "AFEnums.h"
#include "Biteable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UBiteable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class ANIMALFARM_API IBiteable
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this iteface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Bite Reaction")
	EBiteType GetBiteResponse();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Bite Reaction")
	void ReactToBite(class AAnimalFarmCharacter* bitten_by);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Bite Reaction")
	void ReactToEaten(class AAnimalFarmCharacter* eaten_by);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Bite Reaction")
	void ReactToCarry(class AAnimalFarmCharacter* carried_by);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Bite Reaction")
	void ReactToDrag(class AAnimalFarmCharacter* dragged_by);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Bite Reaction")
	void ReactToDrop(class AAnimalFarmCharacter* dropped_by);
};
