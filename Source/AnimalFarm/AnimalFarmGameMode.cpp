// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "AnimalFarmGameMode.h"
#include "AnimalFarmCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAnimalFarmGameMode::AAnimalFarmGameMode()
{
	// set default pawn class to our Blueprinted character

	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Pawns/BP_Pig"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
