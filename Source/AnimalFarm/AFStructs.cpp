// Copyright � 2019 Drovean (Samael Volkihar)

#include "AFStructs.h"

const float FPigCustomisationData::MeshScale_Min = 0.5f;
const float FPigCustomisationData::MeshScale_Max = 1.5f;

const float FPigCustomisationData::MatScale_Min = 0.005f;
const float FPigCustomisationData::MatScale_Max = 0.1f;

const float FPigCustomisationData::NoiseOffset_Min = -12.75f;
const float FPigCustomisationData::NoiseOffset_Max = 12.75f;

const float FPigCustomisationData::Hue_Min = 0.f;
const float FPigCustomisationData::Hue_Max = 25.5f;

const float FPigCustomisationData::Sat_Min = 0.f;
const float FPigCustomisationData::Sat_Max = 1.f;

const float FPigCustomisationData::Val_Min = 0.f;
const float FPigCustomisationData::Val_Max = 0.255f;

const float FPigCustomisationData::Opacity_Min = 0.f;
const float FPigCustomisationData::Opacity_Max = 0.95f;