// Copyright © 2019 Drovean (Samael Volkihar)

#pragma once

//#include "AFEnums.generated.h"

UENUM(BlueprintType)
enum class EBiteType : uint8
{
	NONE	UMETA(DisplayName = "None"),
	DAMAGE	UMETA(DisplayName = "Damage"),
	CARRY	UMETA(DisplayName = "Carry"),
	DRAG	UMETA(DisplayName = "Drag"),
	EAT		UMETA(DisplayName = "Eat")
};

UENUM(BlueprintType)
enum class EAFGameUIState : uint8
{
	MAIN_MENU	UMETA(DisplayName = "Main Menu"),
	PAINT_A_PIG	UMETA(DisplayName = "Paint-A-Pig"),
	PLAYING		UMETA(DisplayName = "Playing"),
	PAUSED		UMETA(DisplayName = "Paused"),
	GAME_OVER	UMETA(DisplayName = "Game Over"),
	HELP		UMETA(DisplayName = "Help")
};