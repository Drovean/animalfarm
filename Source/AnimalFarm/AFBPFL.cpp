// Copyright � 2019 Drovean(Samael Volkihar)

#include "AFBPFL.h"

void UAFBPFL::GetPigMeshScaleMin(float& MinScale) { MinScale = FPigCustomisationData::MeshScale_Min; }
void UAFBPFL::GetPigMeshScaleMax(float& MaxScale) { MaxScale = FPigCustomisationData::MeshScale_Max; }

void UAFBPFL::GetPigMatScaleMin(float& MinScale) { MinScale = FPigCustomisationData::MatScale_Min; }
void UAFBPFL::GetPigMatScaleMax(float& MaxScale) { MaxScale = FPigCustomisationData::MatScale_Max; }

void UAFBPFL::GetPigMatOffsetMin(float& MinOffset) { MinOffset = FPigCustomisationData::NoiseOffset_Min; }
void UAFBPFL::GetPigMatOffsetMax(float& MaxOffset) { MaxOffset = FPigCustomisationData::NoiseOffset_Max; }

void UAFBPFL::GetPigMatHueMin(float& MinHue) { MinHue = FPigCustomisationData::Hue_Min; }
void UAFBPFL::GetPigMatHueMax(float& MaxHue) { MaxHue = FPigCustomisationData::Hue_Max; }

void UAFBPFL::GetPigMatSatMin(float& MinSat) { MinSat = FPigCustomisationData::Sat_Min; }
void UAFBPFL::GetPigMatSatMax(float& MaxSat) { MaxSat = FPigCustomisationData::Sat_Max; }

void UAFBPFL::GetPigMatValMin(float& MinVal) { MinVal = FPigCustomisationData::Val_Min; }
void UAFBPFL::GetPigMatValMax(float& MaxVal) { MaxVal = FPigCustomisationData::Val_Max; }

void UAFBPFL::GetPigMatOpacityMin(float& MinOpacity) { MinOpacity = FPigCustomisationData::Opacity_Min; }
void UAFBPFL::GetPigMatOpacityMax(float& MaxOpacity) { MaxOpacity = FPigCustomisationData::Opacity_Max; }

FString UAFBPFL::PigCustomisationValsToHexString(FPigCustomisationData pmd)
{
	return FString::Printf(TEXT("%02X%02X%02X%02X%02X%X%02X%02X%02X%02X%02X%02X%02X%02X"),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(((pmd.MeshScale - FPigCustomisationData::MeshScale_Min) / (FPigCustomisationData::MeshScale_Max - FPigCustomisationData::MeshScale_Min))*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(((pmd.MatScale - FPigCustomisationData::MatScale_Min) / (FPigCustomisationData::MatScale_Max - FPigCustomisationData::MatScale_Min))*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(pmd.NoiseOffset.X + FPigCustomisationData::NoiseOffset_Max), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(pmd.NoiseOffset.Y + FPigCustomisationData::NoiseOffset_Max), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(pmd.NoiseOffset.Z + FPigCustomisationData::NoiseOffset_Max), 0, 255),
		(uint8)pmd.Invert*15,
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(((pmd.HueA - FPigCustomisationData::Hue_Min) / (FPigCustomisationData::Hue_Max - FPigCustomisationData::Hue_Min))*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(((pmd.SatA - FPigCustomisationData::Sat_Min) / (FPigCustomisationData::Sat_Max - FPigCustomisationData::Sat_Min))*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(((pmd.ValA - FPigCustomisationData::Val_Min) / (FPigCustomisationData::Val_Max - FPigCustomisationData::Val_Min))*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(((pmd.OpacityA - FPigCustomisationData::Opacity_Min) / (FPigCustomisationData::Opacity_Max - FPigCustomisationData::Opacity_Min))*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(((pmd.HueB - FPigCustomisationData::Hue_Min) / (FPigCustomisationData::Hue_Max - FPigCustomisationData::Hue_Min))*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(((pmd.SatB - FPigCustomisationData::Sat_Min) / (FPigCustomisationData::Sat_Max - FPigCustomisationData::Sat_Min))*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(((pmd.ValB - FPigCustomisationData::Val_Min) / (FPigCustomisationData::Val_Max - FPigCustomisationData::Val_Min))*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(((pmd.OpacityB - FPigCustomisationData::Opacity_Min) / (FPigCustomisationData::Opacity_Max - FPigCustomisationData::Opacity_Min))*255.f), 0, 255));
}

FString UAFBPFL::PigSliderValsToHexString(float MeshScale, float MatScale,
	float NoiseOffsetX, float NoiseOffsetY, float NoiseOffsetZ, bool Invert,
	float HueA, float SatA, float ValA, float OpacityA,
	float HueB, float SatB, float ValB, float OpacityB)
{
	return FString::Printf(TEXT("%02X%02X%02X%02X%02X%X%02X%02X%02X%02X%02X%02X%02X%02X"),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(MeshScale*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(MatScale*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(NoiseOffsetX*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(NoiseOffsetY*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(NoiseOffsetZ*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(Invert*15.f), 0, 15),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(HueA*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(SatA*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(ValA*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(OpacityA*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(HueB*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(SatB*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(ValB*255.f), 0, 255),
		(uint8)FMath::Clamp<int32>(FMath::RoundToInt(OpacityB*255.f), 0, 255));
}

bool UAFBPFL::HexStringIsValid(const FString In)
{
	if (In.Len() == 26)
	{
		for (int i = 0; i < 26; i++)
		{
			FString currChar = In.Mid(i, 1);
			if (!( currChar.Equals("0")
				|| currChar.Equals("1")
				|| currChar.Equals("2")
				|| currChar.Equals("3")
				|| currChar.Equals("4")
				|| currChar.Equals("5")
				|| currChar.Equals("6")
				|| currChar.Equals("7")
				|| currChar.Equals("8")
				|| currChar.Equals("9")
				|| currChar.Equals("A", ESearchCase::IgnoreCase)
				|| currChar.Equals("B", ESearchCase::IgnoreCase)
				|| currChar.Equals("C", ESearchCase::IgnoreCase)
				|| currChar.Equals("D", ESearchCase::IgnoreCase)
				|| currChar.Equals("E", ESearchCase::IgnoreCase)
				|| currChar.Equals("F", ESearchCase::IgnoreCase)))
				return false;
		}
		return true;
	}
	return false;
}

void UAFBPFL::HexStringToPigSliderVals(const FString In, float& MeshScale, float& MatScale,
	float& NoiseOffsetX, float& NoiseOffsetY, float& NoiseOffsetZ, bool& Invert,
	float& HueA, float& SatA, float& ValA, float& OpacityA,
	float& HueB, float& SatB, float& ValB, float& OpacityB)
{
	MeshScale = FParse::HexNumber(*(In.Left(2))) / 255.f;
	MatScale = FParse::HexNumber(*(In.Mid(2, 2))) / 255.f;
	NoiseOffsetX = FParse::HexNumber(*(In.Mid(4, 2))) / 255.f;
	NoiseOffsetY = FParse::HexNumber(*(In.Mid(6, 2))) / 255.f;
	NoiseOffsetZ = FParse::HexNumber(*(In.Mid(8, 2))) / 255.f;
	Invert = (FParse::HexNumber(*(In.Mid(10, 1))) / 15.f) == 1;
	HueA = FParse::HexNumber(*(In.Mid(11, 2))) / 255.f;
	SatA = FParse::HexNumber(*(In.Mid(13, 2))) / 255.f;
	ValA = FParse::HexNumber(*(In.Mid(15, 2))) / 255.f;
	OpacityA = FParse::HexNumber(*(In.Mid(17, 2))) / 255.f;
	HueB = FParse::HexNumber(*(In.Mid(19, 2))) / 255.f;
	SatB = FParse::HexNumber(*(In.Mid(21, 2))) / 255.f;
	ValB = FParse::HexNumber(*(In.Mid(23, 2))) / 255.f;
	OpacityB = FParse::HexNumber(*(In.Mid(25, 2))) / 255.f;
}

void UAFBPFL::HexStringToPigCustomisationVals(const FString In, float& MeshScale, float& MatScale,
	float& NoiseOffsetX, float& NoiseOffsetY, float& NoiseOffsetZ, bool& Invert,
	float& HueA, float& SatA, float& ValA, float& OpacityA,
	float& HueB, float& SatB, float& ValB, float& OpacityB)
{
	MeshScale = FParse::HexNumber(*(In.Left(2))) / 255.f * (FPigCustomisationData::MeshScale_Max - FPigCustomisationData::MeshScale_Min) + FPigCustomisationData::MeshScale_Min;
	MatScale = FParse::HexNumber(*(In.Mid(2, 2))) / 255.f * (FPigCustomisationData::MatScale_Max - FPigCustomisationData::MatScale_Min) + FPigCustomisationData::MatScale_Min;
	NoiseOffsetX = FParse::HexNumber(*(In.Mid(4, 2))) / 255.f * (FPigCustomisationData::NoiseOffset_Max - FPigCustomisationData::NoiseOffset_Min) + FPigCustomisationData::NoiseOffset_Min;
	NoiseOffsetY = FParse::HexNumber(*(In.Mid(6, 2))) / 255.f * (FPigCustomisationData::NoiseOffset_Max - FPigCustomisationData::NoiseOffset_Min) + FPigCustomisationData::NoiseOffset_Min;
	NoiseOffsetZ = FParse::HexNumber(*(In.Mid(8, 2))) / 255.f * (FPigCustomisationData::NoiseOffset_Max - FPigCustomisationData::NoiseOffset_Min) + FPigCustomisationData::NoiseOffset_Min;
	Invert = (FParse::HexNumber(*(In.Mid(10, 1))) / 15.f) == 1;
	HueA = FParse::HexNumber(*(In.Mid(11, 2))) / 255.f * (FPigCustomisationData::Hue_Max - FPigCustomisationData::Hue_Min) + FPigCustomisationData::Hue_Min;
	SatA = FParse::HexNumber(*(In.Mid(13, 2))) / 255.f * (FPigCustomisationData::Sat_Max - FPigCustomisationData::Sat_Min) + FPigCustomisationData::Sat_Min;
	ValA = FParse::HexNumber(*(In.Mid(15, 2))) / 255.f * (FPigCustomisationData::Val_Max - FPigCustomisationData::Val_Min) + FPigCustomisationData::Val_Min;
	OpacityA = FParse::HexNumber(*(In.Mid(17, 2))) / 255.f * (FPigCustomisationData::Opacity_Max - FPigCustomisationData::Opacity_Min) + FPigCustomisationData::Opacity_Min;
	HueB = FParse::HexNumber(*(In.Mid(19, 2))) / 255.f * (FPigCustomisationData::Hue_Max - FPigCustomisationData::Hue_Min) + FPigCustomisationData::Hue_Min;
	SatB = FParse::HexNumber(*(In.Mid(21, 2))) / 255.f * (FPigCustomisationData::Sat_Max - FPigCustomisationData::Sat_Min) + FPigCustomisationData::Sat_Min;
	ValB = FParse::HexNumber(*(In.Mid(23, 2))) / 255.f * (FPigCustomisationData::Val_Max - FPigCustomisationData::Val_Min) + FPigCustomisationData::Val_Min;
	OpacityB = FParse::HexNumber(*(In.Mid(25, 2))) / 255.f * (FPigCustomisationData::Opacity_Max - FPigCustomisationData::Opacity_Min) + FPigCustomisationData::Opacity_Min;
}

FPigCustomisationData UAFBPFL::HexStringToPigCustomisationData(const FString In)
{
	return FPigCustomisationData(
		FParse::HexNumber(*(In.Left(2))) / 255.f * (FPigCustomisationData::MeshScale_Max - FPigCustomisationData::MeshScale_Min) + FPigCustomisationData::MeshScale_Min,
		FParse::HexNumber(*(In.Mid(2, 2))) / 255.f * (FPigCustomisationData::MatScale_Max - FPigCustomisationData::MatScale_Min) + FPigCustomisationData::MatScale_Min,
		FVector(
			FParse::HexNumber(*(In.Mid(4, 2))) / 255.f * (FPigCustomisationData::NoiseOffset_Max - FPigCustomisationData::NoiseOffset_Min) + FPigCustomisationData::NoiseOffset_Min,
			FParse::HexNumber(*(In.Mid(6, 2))) / 255.f * (FPigCustomisationData::NoiseOffset_Max - FPigCustomisationData::NoiseOffset_Min) + FPigCustomisationData::NoiseOffset_Min,
			FParse::HexNumber(*(In.Mid(8, 2))) / 255.f * (FPigCustomisationData::NoiseOffset_Max - FPigCustomisationData::NoiseOffset_Min) + FPigCustomisationData::NoiseOffset_Min),
		(FParse::HexNumber(*(In.Mid(10, 1))) / 15.f) == 1,
		FParse::HexNumber(*(In.Mid(11, 2))) / 255.f * (FPigCustomisationData::Hue_Max - FPigCustomisationData::Hue_Min) + FPigCustomisationData::Hue_Min,
		FParse::HexNumber(*(In.Mid(13, 2))) / 255.f * (FPigCustomisationData::Sat_Max - FPigCustomisationData::Sat_Min) + FPigCustomisationData::Sat_Min,
		FParse::HexNumber(*(In.Mid(15, 2))) / 255.f * (FPigCustomisationData::Val_Max - FPigCustomisationData::Val_Min) + FPigCustomisationData::Val_Min,
		FParse::HexNumber(*(In.Mid(17, 2))) / 255.f * (FPigCustomisationData::Opacity_Max - FPigCustomisationData::Opacity_Min) + FPigCustomisationData::Opacity_Min,
		FParse::HexNumber(*(In.Mid(19, 2))) / 255.f * (FPigCustomisationData::Hue_Max - FPigCustomisationData::Hue_Min) + FPigCustomisationData::Hue_Min,
		FParse::HexNumber(*(In.Mid(21, 2))) / 255.f * (FPigCustomisationData::Sat_Max - FPigCustomisationData::Sat_Min) + FPigCustomisationData::Sat_Min,
		FParse::HexNumber(*(In.Mid(23, 2))) / 255.f * (FPigCustomisationData::Val_Max - FPigCustomisationData::Val_Min) + FPigCustomisationData::Val_Min,
		FParse::HexNumber(*(In.Mid(25, 2))) / 255.f * (FPigCustomisationData::Opacity_Max - FPigCustomisationData::Opacity_Min) + FPigCustomisationData::Opacity_Min);
}