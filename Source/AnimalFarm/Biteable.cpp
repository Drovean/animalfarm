// Copyright � 2019 Drovean (Samael Volkihar)

#include "Biteable.h"

UBiteable::UBiteable(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) { }

// Add default functionality here for any IEdible functions that are not pure virtual.
