// Copyright � 2019 Drovean(Samael Volkihar)

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "AFStructs.h"
#include "AFBPFL.generated.h"

UCLASS()
class ANIMALFARM_API UAFBPFL : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMeshScaleMin(float& MinScale);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMeshScaleMax(float& MaxScale);

	// Pig Material min / max
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatScaleMin(float& MinScale);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatScaleMax(float& MaxScale);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatOffsetMin(float& MinOffset);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatOffsetMax(float& MaxOffset);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatHueMin(float& MinHue);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatHueMax(float& MaxHue);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatSatMin(float& MinSat);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatSatMax(float& MaxSat);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatValMin(float& MinVal);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatValMax(float& MaxVal);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatOpacityMin(float& MinOpacity);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void GetPigMatOpacityMax(float& MaxOpacity);

	// Data translations
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static FString PigCustomisationValsToHexString(FPigCustomisationData pmd);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static FString PigSliderValsToHexString(float MeshScale, float MatScale,
		float NoiseOffsetX, float NoiseOffsetY, float NoiseOffsetZ, bool Invert,
		float HueA, float SatA, float ValA, float OpacityA,
		float HueB, float SatB, float ValB, float OpacityB);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static bool HexStringIsValid(const FString In);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void HexStringToPigSliderVals(const FString In, float& MeshScale, float& MatScale,
		float& NoiseOffsetX, float& NoiseOffsetY, float& NoiseOffsetZ, bool& Invert,
		float& HueA, float& SatA, float& ValA, float& OpacityA,
		float& HueB, float& SatB, float& ValB, float& OpacityB);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static void HexStringToPigCustomisationVals(const FString In, float& MeshScale, float& MatScale,
		float& NoiseOffsetX, float& NoiseOffsetY, float& NoiseOffsetZ, bool& Invert,
		float& HueA, float& SatA, float& ValA, float& OpacityA,
		float& HueB, float& SatB, float& ValB, float& OpacityB);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PigMaterialData")
	static FPigCustomisationData HexStringToPigCustomisationData(const FString In);
};

