// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "AnimalFarmGameState.generated.h"

/**
 * 
 */
UCLASS()
class ANIMALFARM_API AAnimalFarmGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	/** Called when any Edible is eaten */
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Edible Eaten by Player"))
	void EdibleEaten(const FString& item_eaten, int score_inc, float time_inc);
};
