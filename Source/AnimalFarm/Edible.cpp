// Copyright � 2019 Drovean (Samael Volkihar)

#include "Edible.h"
#include "AnimalFarmCharacter.h"
#include "Components/MeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "DestructibleComponent.h"
#include "Engine/World.h"
#include "AnimalFarmGameState.h"

AEdible::AEdible()
{ PrimaryActorTick.bCanEverTick = false; }

EBiteType AEdible::GetBiteResponse_Implementation()
{ return EBiteType::EAT; }

void AEdible::ReactToEaten_Implementation(AAnimalFarmCharacter* eaten_by)
{
	UDestructibleComponent* dmc = GetDestructibleMesh();

	if (dmc != nullptr)
	{
		TArray<FOverlapInfo> overlaps;
		USphereComponent* bite_col = eaten_by->GetBiteCollider();
		bite_col->GetOverlapsWithActor(this, overlaps);
		FVector bite_loc = bite_col->GetComponentLocation();
		float closest_dist_sqd = FLT_MAX;
		int closest_chunk_index = -1;

		for (FOverlapInfo overlap : overlaps)
		{
			if (!dmc->IsBoneHidden(overlap.OverlapInfo.Item))
			{
				float dist_sqd = FVector::DistSquared(bite_loc, overlap.OverlapInfo.Location);
				if (dist_sqd < closest_dist_sqd)
				{
					closest_dist_sqd = dist_sqd;
					closest_chunk_index = overlap.OverlapInfo.Item;
				}
			}
		}

		if (closest_chunk_index != -1)
		{
			AAnimalFarmGameState* afgs = Cast<AAnimalFarmGameState>(GetWorld()->GetGameState());
			if(afgs)
				afgs->EdibleEaten(name, score_value, time_value);

			dmc->SetChunkVisible(closest_chunk_index, false);
			dmc->GetBodyInstance(dmc->GetBoneName(closest_chunk_index))->SetInstanceSimulatePhysics(false);
		}
	}
	else
	{
		AAnimalFarmGameState* afgs = Cast<AAnimalFarmGameState>(GetWorld()->GetGameState());
		if (afgs)
			afgs->EdibleEaten(name, score_value, time_value);

		Destroy(this);
	}
}